---
title: RPIE Outreach Report
output: html_document
---

```{r setup, include = FALSE, echo = FALSE}

knitr::opts_chunk$set(
  echo = FALSE, 
  fig.align = "center",
  
  warning = FALSE,
  message = FALSE
)

# source reporting ingest script
source(paste0(
  "C:/Users/",
  Sys.info()[["user"]],
  "/Documents/commercial-apartments-automated-valuation-model/code.r/reporting/valuation_report_ingest.r")
  )

# create CCAODATA connection object
CCAODATA <- dbConnect(odbc(), .connection_string = Sys.getenv("DB_CONFIG_CCAODATA"))

```

<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.5/css/jquery.dataTables.min.css">
<script src="http://code.jquery.com/jquery-2.1.2.min.js"></script>
<script src="http://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
         $(document).ready(function() {
             $("table").DataTable();
         } );
</script>

```{r, results = 'asis'}

dbGetQuery(CCAODATA,
           "SELECT township_name AS Town, HD_NBHD AS Neighborhood, HD_CLASS AS Class, SUM(CASE WHEN rpie_filing IS NULL THEN 0 ELSE rpie_filing END) AS [RPIE Filings], COUNT(HEAD.PIN) AS PINs FROM AS_HEADT HEAD

LEFT JOIN FTBL_TOWNCODES ON LEFT(HD_TOWN, 2) = township_code

LEFT JOIN

    (SELECT DISTINCT PinNumber, 1 AS rpie_filing FROM [CCAOAPPSRV].[RPIE].[dbo].[Pin] P

    LEFT JOIN [CCAOAPPSRV].[RPIE].[dbo].[FilingPin] FP ON P.PinId = FP.PinId

    WHERE FP.FilingId IS NOT NULL) RPIE

ON HEAD.PIN = RPIE.PinNumber

WHERE TAX_YEAR = 2019
AND HD_CLASS >= 300
GROUP BY HD_CLASS, HD_NBHD, township_name
ORDER BY Town, Neighborhood, Class"
           ) %>%
  mutate(`Response Rate` = label_percent(accuracy = 1)(`RPIE Filings` / PINs)) %>%
  kable() %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed", "responsive"),
                position = "center", full_width = TRUE)

```