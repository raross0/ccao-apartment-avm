# This file calls other R scripts to execute the automated valuation pipeline for
# commercial apartments. This can be run in parallel to the file 'Valuation Report.Rmd'

# create parameter list as in markdown file
params <- c("fetch" = TRUE,
            "impute_missing_train" = TRUE,
            "impute_missing_sample" = FALSE,
            # replace valuations data with RPIE data if it's available
            "rpie_replace_ccao" = TRUE,
            # append trueroll's scraped apartment listings to RPIE data for training
            "exemption_project" = TRUE,
            # limit to validated commercial sales
            "valid" = FALSE,
            # method_318 can be set to "ratios" or "com_rents"
            "method_318" = "com_rents",
            # limit latest submission date for RPIE data
            # date must be entered as a character string in YYYY-MM-DD format, i.e. "2020-07-23"
            "limit_date" = NA)

# sourcing CAMA scripts
source(paste0("C:/Users/", Sys.info()[["user"]], "/Documents/commercial-apartments-automated-valuation-model/code.r/setup.R"))

if (params["fetch"]) {

  if (params["exemption_project"]) source('code.r/0a_ingest_third_party_apt_data.R')
  source('code.r/0b_ingest_sql_and_census_data.R')
  source('code.r/0c_ingest_ic_excel_sheets.R')
  source('code.r/0d_ingest_trepp_costar.R')
  source('code.r/0e_ingest_schedule_es.R')
  source('code.r/0f_ingest_8825s.R')

}

source("code.r/1_summary_statistics.R")
source("code.r/2_predict_rents.R")
source("code.r/3_predict_rental_nonpayment.R")
source("code.r/4_predict_vacancy.R")
source("code.r/5_construct_expense_ratios.R")
source("code.r/6_calculate_cap_rates.R")
source("code.r/7_calculate_fmv_adjust_318s.R")
source("code.r/8_export.R")
